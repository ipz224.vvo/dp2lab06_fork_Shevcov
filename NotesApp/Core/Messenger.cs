﻿namespace NotesApp.Core;

public class Messenger
{
    private static Messenger _instance;
    public static Messenger Instance => _instance ??= new Messenger();

    private int _counter = 0;
    private readonly Dictionary<string, Dictionary<int, Action<object>>> _actions = new();

    /// <summary>
    /// Registers a new action for the token.
    /// </summary>
    /// <returns>ID for new action.</returns>
    public int Register(string token, Action<object> callback)
    {
        if (!_actions.ContainsKey(token))
            _actions[token] = new Dictionary<int, Action<object>>();
        _actions[token][_counter++] = callback;
        return _counter - 1;
    }

    /// <summary>
    /// Removes all actions for specified token.
    /// </summary>
    public void Unregister(string token)
        => _actions.Remove(token);

    /// <summary>
    /// Removes specific action.
    /// </summary>
    public void Unregister(string token, int id)
    {
        if (_actions.TryGetValue(token, out var actions))
            actions.Remove(id);
    }

    /// <summary>
    /// Notify all listeners of specified token.
    /// </summary>
    public void Send(string token, object param = null)
    {
        if (_actions.TryGetValue(token, out var actions))
            foreach (var action in actions.Values)
                action.Invoke(param);
    }
}