﻿using NotesApp.MVVM.Models;

namespace NotesApp.Core;

public class ImprovisedDatabase
{
    private static ImprovisedDatabase _instance;
    public static ImprovisedDatabase Instance => _instance ??= new ImprovisedDatabase();

    private readonly List<Note> _notes = [];

    public Note AddNote(string title, string content, DateTime? alarm = null)
    {
        var note = new Note
        {
            Title = title,
            Content = content,
            AlarmAt = alarm
        };
        _notes.Add(note);
        return note;
    }

    public bool RemoveNote(Guid id)
        => _notes.Remove(FindById(id));

    public IEnumerable<Note> GetNotes() => new List<Note>(_notes);

    public Note? FindById(Guid id) => _notes.FirstOrDefault(n => n.Id.Equals(id), null);
}
