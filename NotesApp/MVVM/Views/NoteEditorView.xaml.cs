﻿using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit;

namespace NotesApp.MVVM.Views;

public partial class NoteEditorView : UserControl
{
    public NoteEditorView()
    {
        InitializeComponent();
    }

    private void AlarmDateTimePicker_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
        var picker = (DateTimePicker)sender;
        if (picker.Value is null)
        {
            AlarmStackPanel.Visibility = Visibility.Collapsed;
            SetAlarmButton.Visibility = Visibility.Visible;
        }
        else
        {
            SetAlarmButton.Visibility = Visibility.Collapsed;
            AlarmStackPanel.Visibility = Visibility.Visible;
        }
    }
}
