﻿using Microsoft.Toolkit.Uwp.Notifications;
using NotesApp.Core;
using NotesApp.MVVM.Models;

namespace NotesApp.MVVM.ViewModels;

public class MainViewModel : ObservableObject
{
    private object _view;
    private Timer _alarmCheckTimer;
    private List<Note> _alarmNotes;

    public object View
    {
        get => _view;
        set
        {
            _view = value;
            OnPropertyChanged();
        }
    }

    public NotesViewModel NotesViewModel;
    public NoteEditorViewModel NoteEditorViewModel;

    public MainViewModel()
    {
        View = NotesViewModel = new NotesViewModel();
        NoteEditorViewModel = new NoteEditorViewModel();

        Messenger.Instance.Register("NoteListUpdate", _ => UpdateCheckedNotesList());
        Messenger.Instance.Register("GoHome", _ => View = NotesViewModel);
        Messenger.Instance.Register("OpenNote", o =>
        {
            if (o is not Note note) return;
            NoteEditorViewModel.SetNote(note.Id);
            View = NoteEditorViewModel;
        });

        UpdateCheckedNotesList();
        _alarmCheckTimer = new Timer(CheckAlarmsCallback, null, TimeSpan.Zero, 
            new TimeSpan(0,0,1));
    }

    private void CheckAlarmsCallback(object? state)
    {
        var now = DateTime.Now;
        foreach (var note in _alarmNotes.Where(note => note.AlarmAt <= now).ToList())
        {
            new ToastContentBuilder()
                .AddText("Note Alarm")
                .AddText($"It's time to check your note:\n{note.Title}")
                .SetToastScenario(ToastScenario.Reminder)
                .Show();
            _alarmNotes.Remove(note);
        }
    }

    private void UpdateCheckedNotesList()
    {
        var now = DateTime.Now;
        _alarmNotes = ImprovisedDatabase.Instance.GetNotes()
            .Where(n => n.AlarmAt is not null && n.AlarmAt > now)
            .ToList();
    }
}
