﻿using NotesApp.Core;
using NotesApp.MVVM.Models;

namespace NotesApp.MVVM.ViewModels;

public class NoteEditorViewModel : ObservableObject
{
    private Note _note;
    
    public string Title
    {
        get => _note.Title;
        set
        {
            _note.Title = value;
            OnPropertyChanged();
        }
    }

    public string Content
    {
        get => _note.Content;
        set
        {
            _note.Content = value;
            OnPropertyChanged();
        }
    }

    public DateTime CreatedAt => _note.CreatedAt;

    public DateTime? AlarmAt
    {
        get => _note.AlarmAt;
        set
        {
            _note.AlarmAt = value;
            OnPropertyChanged();
            Messenger.Instance.Send("NoteListUpdate", _note);
        }
    }
    
    public RelayCommand GoHomeCommand { get; set; }
    public RelayCommand RemoveAlarmCommand { get; set; }
    public RelayCommand SetAlarmCommand { get; set; }

    public NoteEditorViewModel()
    {
        GoHomeCommand = new RelayCommand(_ => Messenger.Instance.Send("GoHome"));
        RemoveAlarmCommand = new RelayCommand(_ => AlarmAt = null);
        SetAlarmCommand = new RelayCommand(_ => AlarmAt = DateTime.Today);
    }
        
    public void SetNote(Guid id)
    {
        var note = ImprovisedDatabase.Instance.FindById(id);
        _note = note;
        OnPropertyChanged(null);
    }
}
