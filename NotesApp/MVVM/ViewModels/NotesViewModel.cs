﻿using System.Collections.ObjectModel;
using NotesApp.Core;
using NotesApp.MVVM.Models;

namespace NotesApp.MVVM.ViewModels;

public class NotesViewModel : ObservableObject
{
    public ObservableCollection<Note> Notes { get; set; }
    public RelayCommand OpenNoteCommand { get; set; }
    public RelayCommand AddNewNoteCommand { get; set; }
    public RelayCommand DeleteNoteCommand { get; set; }

    public NotesViewModel()
    {
        Notes = [..ImprovisedDatabase.Instance.GetNotes()];

        OpenNoteCommand = new RelayCommand(o =>
        {
            if (o is not Note note) return;
            Messenger.Instance.Send("OpenNote", note);
        });
        
        AddNewNoteCommand = new RelayCommand(_ =>
        {
            var note = ImprovisedDatabase.Instance.AddNote("New Note", string.Empty);
            Messenger.Instance.Send("OpenNote", note);
        });

        DeleteNoteCommand = new RelayCommand(o =>
        {
            if (o is not Note note) return;
            Notes.Remove(Notes.FirstOrDefault(n => n.Id.Equals(note.Id), null));
            ImprovisedDatabase.Instance.RemoveNote(note.Id);
            Messenger.Instance.Send("NoteListUpdate", note);
        });
    }
}
