# [Лабораторна робота №6](https://learn.ztu.edu.ua/mod/assign/view.php?id=201665)

## Електронний записник з нагадуваннями

**Мета:** навчитися слідувати принципам чистого коду та
реалізовувати потрібні патерни програмування в програмному коді.

- [Project Info](#user-content-project-info)
- [Programming Principles](#user-content-programming-principles)
- [Design Patterns](#user-content-design-patterns)
- [Architectural Patterns](#user-content-architectural-patterns)
- [Refactoring Techniques](#user-content-refactoring-techniques)

---

### Project Info

This is a project for creating your own Notes with alarms. It's a small project, there is no Database here, we
use a singleton object [`ImprovisedDatabase`](NotesApp/Core/ImprovisedDatabase.cs) to store all notes in memory, so they're not saved after exit.

While the app is running, it constantly checks for existing notes with alarms and when the time comes,
a toast notification pops up to notify the user.

This project built on WPF with MVVM pattern. You can find business logic in [MVVM/ViewModels](NotesApp/MVVM/ViewModels) 
and all bindings in [Views](NotesApp/MVVM/Views) XAML files.

---

### Programming Principles
<!-- Fill additional info here as development goes by --->
1. **YAGNI** - In this project we use only necessary objects. All redundant code (namespaces, classes, methods, invokes)
   are avoided and should be cleaned up after the project is finished.
2. **KISS** - Along with **MVVM pattern**, this principle is easy to implement.
   We have 3 main objects which operate the app. Their code is pretty easy to read and simple enough to understand.
3. **DRY** - Any repeated samples of code are separated to different functions to get rid of repeats and
   provide simple of use and readability of code.
4. **SOLID**
   - _**S**ingle Responsibility_ - Achieved with **MVVM pattern**, each `Model`, `View` and `ViewModel`
     have their own purpose and are not reused for different features.
   - _**O**pen/Closed_ - In this project we use only extend existing classes without changing their original behaviour.
     For example: [`ObservableObject`](NotesApp/Core/ObservableObject.cs) is inherited by ViewModels,
     which provide additional behaviour without interrupting original abstraction.
   - _**L**iskov Substitution_ - As mentioned in **Open/Closed** principle, we use `ObservableObject` that implements
     `INotifyPropertyChanged` interface, which is used by WPF to update View on Model changes. It means, we can use
     any model as a representation for our view.
   - _**I**nterface Segregation_ - Yet again, **MVVM** includes this principle, WPF rely on Interfaces which we provide
     with Models.
   - _**D**ependency Inversion_ - And again - **MVVM**.

---

### Design Patterns

1. **Mediator** - We use [`Messenger`](NotesApp/Core/Messenger.cs) as a mediator between ViewModels to subscribe and
   listen for notifications from another ViewModel, so we can change main view for example.
2. **Observer** - [`Messenger`](NotesApp/Core/Messenger.cs) also act's as observer, allowing multiple objects to subscribe
   to the specific event and prefer actions whenever event is invoked.
3. **Singleton** - Both [`Messenger`](NotesApp/Core/Messenger.cs) and [`ImprovisedDatabase`](NotesApp/Core/ImprovisedDatabase.cs) 
   are also implemented with singleton providing only one existing instance in the app.
4. **Command** - In XAML we use [`RelayCommand`](NotesApp/Core/RelayCommand.cs) to create commands in ViewModel
   and pass them as objects to the View. 

---

### Architectural Patterns

1. **MVVM (Model-View-ViewModel)** - Is a great choice for WPF projects which mostly rely on observable objects that
   implement `INotifyPropertyChanged` to update View when ModelView is changed. The application has only one active
   `Window` that changes Views with `ContentControl`. This pattern significantly improves clean of code and provides
   easier management for the project, all MVVM assets are based in [MVVM folder](NotesApp/MVVM) and managed by
   [`MainViewModel`](NotesApp/MVVM/ViewModels/MainViewModel.cs) with [`MainWindow`](NotesApp/MainWindow.xaml).

---

### Refactoring Techniques

1. -

---
